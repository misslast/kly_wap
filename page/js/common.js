var plus = window.plus;
//取消浏览器的所有事件，使得active的样式在手机上正常生效
document.addEventListener('touchstart',function(){
    return false;
},true);
// 禁止选择
document.oncontextmenu=function(){
	return false;
};
// H5 plus事件处理
var as='pop-in';// 默认窗口动画
function plusReady(){
	if(window.plus){
		// 隐藏滚动条
		window.plus.webview.currentWebview().setStyle({scrollIndicator:'none'});
		// Android处理返回键
		window.plus.key.addEventListener('backbutton',function(){
			window.back();
		},false);
		compatibleAdjust();
	}
	
}
if(window.plus){
	plusReady();
}else{
	document.addEventListener('plusready',plusReady,false);
}
// DOMContentLoaded事件处理
var _domReady=false;
document.addEventListener('DOMContentLoaded',function(){
	_domReady=true;
	compatibleAdjust();
},false);
// 兼容性样式调整
var _adjust=false;
function compatibleAdjust(){
	if(_adjust||!window.plus||!_domReady){
		return;
	}
	_adjust=true;
	if(window.plus){
		// iOS平台特效
		if('iOS'==window.plus.os.name){
			document.getElementById('content').className='scontent';	// 使用div的滚动条
			if(navigator.userAgent.indexOf('StreamApp')>=0){	// 在流应用模式下显示返回按钮
				document.getElementById('back').style.visibility='visible';
			}
		}
		// 预创建二级窗口
	//	preateWebviews();
		// 关闭启动界面
		setTimeout(function(){
			window.plus.navigator.closeSplashscreen();
			window.plus.navigator.setStatusBarBackground('#FFFFFF');
			if(window.plus.navigator.isImmersedStatusbar()){
				window.plus.navigator.setStatusBarStyle('UIStatusBarStyleBlackOpaque');
			}
		},500);
	}
	
}


var urls = {
'changyongxinxi.html':1,
'chehuquerenhuozhuyundan.html':1,
'chenzhuweiqueren-huozhuxiugaidingdan.html':1,
'chezhufabu.html':1,
'chezhufabubianji.html':1,
'chezhuquerenhuozhuquxiaotanchuang.html':1,
'chezhurenzheng.html':1,
'chezhuyundanquerenfache.html':1,
'denglu.html':1,
'duihua.html':1,
'haoyou.html':1,
'huozhurenzheng.html':1,
'huozhuyundanquerenfasheng.html':1,
'huozhuyundanquerenjiedan.html':1,
'qiyehuozhurenzheng-fuceng.html':1,
'qiyehuozhurenzheng.html':1,
'shouye.html':1,
'tijiaoyundan.html':1,
'tijiaoyundanhuozhuxiugai.html':1,
'wo.html':1,
'wuliugongsi.html':1,
'wuliugongsirenzheng.html':1,
'xianshangzhaoche-shaixuan.html':1,
'xianshangzhaoche.html':1,
'xiaoxi.html':1,
'yundan.html':1,
'yundanfachexiangqing.html':1,
'zhengchexiangqing.html':1,
'zhuanxianguanli-tianjia.html':1,
'zhuanxianguanli.html':1,
'zhuanxianyunshu.html':1,
'zhuce.html':1,
'ziliaowanshang.html':1
}

window.Storage = {
	set:function(key,value){
		if(window.plus){
			window.plus.storage.setItem(key,value);
		}else{
			window.localStorage.setItem(key,value);
		}
		
	},
	get:function(key){
		if(window.plus){
			window.plus.storage.getItem(key);
		}else{
			return window.localStorage.getItem(key);
		}
		
	}
}

$(function(){
	Storage.set("type_sel","chezhu");
	fixedHeader();
	function fixedHeader(){
		$('.g_header').css({
			'position':'fixed',
			'z-index':9999,
			'width':'100%'
		});

		// $('.g_main').css({
		// 	'paddingTop':'44px'
		// });

	}
	
	$('body').on("click",".openwebview",function(){
		
		var $this = $(this);
		var url = $this.data("url");
		
		if(!urls[url])url = "blank.html";
		clicked(url)
	});

	$('.icon_goBack').click(function(){
		back(true)
	})
	$('.btn_pop_cancel').click(function(){
		back(true)
	})

	// 处理返回事件
	window.back=function(hide){
		if(window.plus){
			var ws=window.plus.webview.currentWebview();
			ws.hide('auto');
		}else if(history.length>1){
			history.back();
		}else{
			window.close();
		}

	};
})


var preate={};
// 处理点击事件
var _openw=null;
function clicked(id,a,s){
	
	if(!window.plus){

		window.location.href = id;
		return;
	}
	
	if(_openw){return;}
	a||(a=as);
	_openw=preate[id];
	if(_openw){
		_openw.showded=true;
		_openw.show(a,null,function(){
			_openw=null;//避免快速点击打开多个页面
		});
	}else{
//		var wa=plus.nativeUI.showWaiting();
		_openw=window.plus.webview.create(id,id,{scrollIndicator:'none',scalable:false,popGesture:'hide'},{preate:true});
		preate[id]=_openw;
		_openw.addEventListener('loaded',function(){//叶面加载完成后才显示
//		setTimeout(function(){//延后显示可避免低端机上动画时白屏
//			wa.close();
			_openw.showded=true;
			s||_openw.show(a,null,function(){
				_openw=null;//避免快速点击打开多个页面
			});
			s&&(_openw=null);//避免s模式下变量无法重置
//		},10);
		},false);
		_openw.addEventListener('hide',function(){
			_openw&&(_openw.showded=true);
			_openw=null;
		},false);
		_openw.addEventListener('close',function(){//页面关闭后可再次打开
			_openw=null;
			preate[id]&&(preate[id]=null);//兼容窗口的关闭
		},false);
	}
}
